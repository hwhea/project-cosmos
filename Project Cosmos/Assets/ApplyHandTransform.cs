﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyHandTransform : MonoBehaviour
{
    [SerializeField]
    private HandTransformState hts;

    [SerializeField]
    GameObject handToTransform;

    // Start is called before the first frame update
    void Start()
    {
        if (handToTransform)
        {
            hts.ApplyRotations(hts.postChangeRotations, handToTransform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
