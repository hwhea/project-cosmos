﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FireType", menuName = "Redwire/Items/FireType")]

public class FireType : ScriptableObject
{
    public string typeName = "";
    [TextArea]
    public string developerDescription = "";
}
