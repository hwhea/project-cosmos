﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponStats", menuName = "Redwire/Items/WeaponStats")]
public class WeaponStats : ScriptableObject
{
    [Header("Damage")]
    public float damagePerHit = 1f; 
    public float refireTime = 0.5f;
    public List<FireType> fireTypes;

}
