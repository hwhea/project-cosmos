﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "String Value", menuName = "Redwire/Data Types/String")]
[System.Serializable]
public class StringValue : ScriptableObject
{
    public string value;


    public override string ToString() {
        return this.value;
    }

    // A person is uniquely identified by name, so let's use it for equality.
    // For lazyness reasons we (incorrectly) use the age as the hash code.
    
}


