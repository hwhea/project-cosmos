﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Integer Value", menuName = "Redwire/Data Types/Integer")]
[System.Serializable]
public class IntValue : ScriptableObject
{
    public int value = 0;
}
