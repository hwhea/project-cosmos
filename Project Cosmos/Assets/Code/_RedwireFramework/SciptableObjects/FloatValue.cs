﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Float Value", menuName = "Redwire/Data Types/Float")]

public class Float : ScriptableObject
{
    public float value;
}
