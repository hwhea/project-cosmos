﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO change this to something else.
[System.Serializable]
[CreateAssetMenu(fileName = "Hand Transform State", menuName = "Redwire/RIVR/HTS")]
public class HandTransformState : ScriptableObject
{
    [SerializeField]
    public Dictionary<HandTransformState.Finger, Vector3[]> preChangeRotations = new Dictionary<Finger, Vector3[]>();
    [SerializeField]
    public  Dictionary<HandTransformState.Finger, Vector3[]> postChangeRotations = new Dictionary<Finger, Vector3[]>();
    public string teststring = "";

    //Trigger here

    public void SetFingers(GameObject og, Dictionary<HandTransformState.Finger, Vector3[]> varToSet)
    {
        varToSet.Clear();
        foreach (Transform child in og.transform)
        {
            //This is each finger

            Debug.Log(child.name);

            AddFingerByName(child, varToSet);

        }

    }

    public Finger StringToFingerType(string str)
    {

        Finger finger = Finger.Thumb;
        if (str.ToLower().Contains("pinky"))
        {
            finger = Finger.Pinky;
        }
        else if (str.ToLower().Contains("mid"))
        {
            finger = Finger.Middle;
        }
        else if (str.ToLower().Contains("index"))
        {
            finger = Finger.Index;
        }
        else if (str.ToLower().Contains("ring"))
        {
            finger = Finger.Ring;
        }
        else if (str.ToLower().Contains("thumb"))
        {
            finger = Finger.Thumb;
        }
        return finger;
    }

    public void ApplyRotations(Dictionary<HandTransformState.Finger, Vector3[]> varToSet, GameObject target)
    {
        Debug.Log(teststring);

        foreach (Transform child in target.transform)
        {
            //This is each finger
            Finger type = StringToFingerType(child.name);

            child.localEulerAngles = varToSet[type][0];
            child.GetChild(0).localEulerAngles = varToSet[type][1];
            child.GetChild(0).GetChild(0).localEulerAngles = varToSet[type][2];
        }
    }


    public void AddFingerByName(Transform root, Dictionary<HandTransformState.Finger, Vector3[]> varToSet)
    {
        Finger finger = StringToFingerType(root.name);
            varToSet.Add(finger, new Vector3[] { new Vector3(root.localEulerAngles.x, root.localEulerAngles.y, root.localEulerAngles.z),
                                                            new Vector3(root.GetChild(0).localEulerAngles.x, root.GetChild(0).localEulerAngles.y, root.GetChild(0).localEulerAngles.z),
                                                             new Vector3(root.GetChild(0).GetChild(0).localEulerAngles.x, root.GetChild(0).GetChild(0).localEulerAngles.y, root.GetChild(0).GetChild(0).localEulerAngles.z)});
    }

    public void SetOriginals(GameObject reference)
    {
        preChangeRotations.Clear();

        SetFingers(reference, preChangeRotations);
    }

    public void SetNew(GameObject reference)
    {
        postChangeRotations.Clear();
        SetFingers(reference, postChangeRotations);
    }

    public enum Finger
    {
        Index,
        Thumb,
        Middle,
        Ring,
        Pinky
    }
}
