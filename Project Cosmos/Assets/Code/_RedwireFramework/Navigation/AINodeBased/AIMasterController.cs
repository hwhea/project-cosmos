﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMasterController : MonoBehaviour
{
    /*
     * Get all AI instances, divide them into different lanes based on last rounds fitness.
     */

    public GameObject enemyPrefab;
    public List<GameObject> aiList = new List<GameObject>();

    float test_spawnDelta = 3f;

    // Start is called before the first frame update
    void Start()
    {
        //here we want to give each ai in our list a path.
        //TODO check if findallobjectsoftype is best way to do this.
        SpawnUnit();
        //FindObjectOfType<AINavigator>().nodePath = DetermineFollowNodes();
    }

    // Update is called once per frame
    void Update()
    {
        //test code
        if (test_spawnDelta < 0f)
        {
            SpawnUnit();
            test_spawnDelta = 0.5f;
        }
        else
        {
            test_spawnDelta -= Time.deltaTime;
        }

    }

    public void SpawnUnit()
    {
        //GameObject go = Instantiate(enemyPrefab, FindObjectOfType<EnemySpawnPoint>().GetRandomPoint(), Quaternion.identity, null);
        //aiList.Add(go);
        //go.GetComponent<AINavigator>().nodePath = DetermineFollowNodes();
        //Debug.Log("Set node path for " + go.name);
    }

    //This function will set a path for a given AI.
    //Add functionality here for balancing etc.
    public static List<AINavigationNode> DetermineFollowNodes()
    {
        Debug.Log("----------------------------------");
        List<AINavigationNode> nodesToFollow = new List<AINavigationNode>();
        RCreateNodePath(AINavigationNode.Network[0], nodesToFollow);
        return nodesToFollow;
    }

    static void RCreateNodePath(AINavigationNode node, List<AINavigationNode> nodesToFollow)
    {
        AINavigationNode thisNode;
        nodesToFollow.Add(node);
        if (node.children.Count > 0)
        {
            if (node.children.Count > 1)
            {
                int i = Random.Range(0, node.children.Count);
                Debug.Log("Choosing child at index " + i);
            
                thisNode = node.children[i];
            }
            else
            {
                thisNode = node.children[0];
            }

            Debug.Log("Added node " + thisNode.gameObject.name);

            RCreateNodePath(thisNode, nodesToFollow);
        }
        else
        {
            Debug.Log("Reached max node depth");
            return;
        }
        

    }
}
