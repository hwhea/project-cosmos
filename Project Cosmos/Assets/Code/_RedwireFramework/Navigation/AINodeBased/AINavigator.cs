﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class AINavigator : MonoBehaviour
{

    NavMeshAgent agent;
    public List<AINavigationNode> nodePath { get; private set; }
    int currentNode = -1;
    Vector3 targetPosition;
    public bool hasFinishedNavigation { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        //Give the AI agent a name for debugging help.
        name = "AI - "+Random.Range(10000, 99000) + "";
        hasFinishedNavigation = false;

        agent = GetComponent<NavMeshAgent>();
        if (!agent)
            Debug.LogError("AI " + name + " doesn't have NavMeshAgent");

        
    }

    public void SetNodePath(List<AINavigationNode> path)
    {
        nodePath = path;
        hasFinishedNavigation = false;
        agent.isStopped = false;
        GoToNextNode();
    }


    // Update is called once per frame
    void Update()
    {
        if (nodePath == null)
            return;

        if(agent.remainingDistance < 0.2f)
        {
            if(currentNode == nodePath.Count - 1)
            {
                //Finished, at objective
                agent.isStopped = true;
                Debug.Log("> " + name + " finished navigation. At node " + currentNode + " of " + nodePath.Count);
                hasFinishedNavigation = true;
            }
            else
            {
                GoToNextNode();
            }
        }
    }

    private void GoToNextNode()
    {
        currentNode += 1;
        //Find a point within the acceptable distance to aim for.
        Vector3 cleanSpherePos = new Vector3(Random.insideUnitSphere.x, 0, Random.insideUnitSphere.z);
        targetPosition = (cleanSpherePos * nodePath[currentNode].proximityRequirement) + nodePath[currentNode].transform.position;
        agent.SetDestination(targetPosition);
    }

    private void OnDrawGizmos()
    {

        if(targetPosition != Vector3.zero)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(transform.position, targetPosition);
            Gizmos.DrawCube(targetPosition, new Vector3(0.2f, 0.2f, 0.2f));
        }
        
        
    }
}
