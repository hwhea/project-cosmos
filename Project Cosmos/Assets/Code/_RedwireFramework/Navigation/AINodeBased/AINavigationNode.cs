﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AINavigationNode : MonoBehaviour
{
    public static List<AINavigationNode> Network = new List<AINavigationNode>();
    public List<AINavigationNode> children = new List<AINavigationNode>();
    public AINavigationNode parent;

    public int survivabilityWeight = 1;

    [Range(0, 100)]
    public float proximityRequirement = 2f;
    void Awake()
    {
        //If parent doesn't have AINavNode property then we are parent. 
        GameObject tparent = transform.parent.gameObject;
        if (tparent.GetComponent<AINavigationNode>() == null)
        {
            Network.Clear();
            Network.Add(this);
            Debug.Log("Added parent node " + name + " to network.");
        }
        else
        {
            parent = tparent.GetComponent<AINavigationNode>();
            parent.GetComponent<AINavigationNode>().children.Add(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDrawGizmos()
    {
        if (transform.parent.gameObject.GetComponent<AINavigationNode>())
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.parent.gameObject.transform.position, gameObject.transform.position);
        }

        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, 0.15f);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, proximityRequirement);
    }

}
