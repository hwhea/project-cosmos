﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class StateMachineDebugger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        foreach(StateMachine sm in FindObjectsOfType<StateMachine>())
        {
            if(sm.CurrentState != null)
            {
                Handles.Label(sm.gameObject.transform.position, sm.CurrentState.ToString());
            }
            else
            {
                Handles.Label(sm.gameObject.transform.position, "Null CurrentState");
            }
            
        }
    }
}
