﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    private Dictionary<Type, BaseState> availableStates;

    public BaseState CurrentState { get; private set; }
    public event Action<BaseState> OnStateChanged;
    BaseState startState = null;

    public void SetStates(Dictionary<Type, BaseState> states, Type startState)
    {
        availableStates = states;
        if(!states.TryGetValue(startState, out this.startState))
        {
            Debug.LogError("Failed to set initial state of " + startState.ToString() + " on FSM. Ensure it is passed in states dictionary.");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    // Update is called once per frame
    void Update()
    {
        if(CurrentState == null)
        {
            CurrentState = this.startState;
        }

        var nextState = CurrentState?.Tick();

        if(nextState != null && nextState != CurrentState?.GetType())
        {
            SwitchToNewState(nextState);
        }

    }

    void SwitchToNewState(Type nextState)
    {
        CurrentState = availableStates[nextState];
    }
}
