﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateTemplate : BaseState
{

    //Here we can pass in the object that this state relates to.
    //For example, for a droid unit:
    //public ShootingState(Droid droid) : base(droid.gameObject)
    public StateTemplate(DemoStateObject dso) : base(dso.gameObject)
    {

    }

    //All logic for the state goes here.
    public override Type Tick()
    {
        throw new NotImplementedException();
        //All logic for the state goes here, to change state you do:
        //return typeof(StateClass);
        //Example:
        //return typeof(ChaseState);
    }

     
}
