﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(StateMachine))]
public class DemoStateObject : MonoBehaviour
{

    StateMachine stateMachine => GetComponent<StateMachine>();


    private void Start()
    {
        InitStateMachine();
    }

    //Useful to have an init statemachine method.
    private void InitStateMachine()
    {
        var states = new Dictionary<Type, BaseState>()
        {
            {typeof(StateTemplate), new StateTemplate(this) }
        };

        //Pass the states variable we created and the 'default state' of the object.
        stateMachine.SetStates(states, typeof(StateTemplate));
    }

    //Example
    public void FireWeapon()
    {
        //Call this from our states, all core functionality is here. Behaviour is NOT. Behaviour belongs in state machine.
    }
}
