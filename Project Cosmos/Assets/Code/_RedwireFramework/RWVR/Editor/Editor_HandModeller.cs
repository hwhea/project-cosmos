﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Editor_HandModeller : EditorWindow
{

    [MenuItem("Redwire/VR/Hand Modeller")]
    public static void ShowWindow()
    {
        GetWindow<Editor_HandModeller>("Map Data Generator");
        
    }

    string[] help = new string[]
    {
        "Give this hand state a friendly name. We suggest weapon_hand_action. Eg: M4A1_right_grip",
        "Choose the hand object and drop it in the window below. Make sure it is active in the scene.",
        "Great, the values from this have been saved. You are now free to manipulate the hands into position.",
        "Below you will find demo values for before and after for the index."
    };

    int step = 0;

    string err = "";
    GUIStyle error = new GUIStyle();

    //props
    string friendlyName = "";
    GameObject handObj;
    HandTransformState hts;


    void SetStep(int nstep)
    {
        step = nstep;
        err = "";
    }

    void OnGUI()
    {
        error.normal.textColor = Color.red;
        EditorGUILayout.LabelField(help[step]);
        EditorGUILayout.LabelField(err, error);
        switch (step)
        {
            case 0:
                hts = (HandTransformState)EditorGUILayout.ObjectField(hts, typeof(HandTransformState));
                if(GUILayout.Button("Use This"))
                {
                    if(hts != null)
                    {
                        if(hts.postChangeRotations.Count != 0)
                        {
                            err = "Not empty" + hts.teststring;
                        }
                        else
                        {
                            SetStep(1);
                        }
                        
                    }
                    else
                    {
                        err = "Invalid HTS object.";
                    }
                    
                }
                break;
            case 1:
                handObj = (GameObject)EditorGUILayout.ObjectField(handObj, typeof(GameObject));
                if (handObj != null)
                {
                    hts.SetOriginals(handObj);
                    SetStep(2);
                }
                    
                break;
            case 2:
                hts.SetNew(handObj);
                if (GUILayout.Button("Reset"))
                {
                    hts.ApplyRotations(hts.preChangeRotations, handObj);
                    SetStep(3);
                }

                if (GUILayout.Button("I'm done!"))
                {
                    hts.SetNew(handObj);
                    SetStep(3);
                }
                break;
            case 3:
                EditorGUILayout.LabelField("Target: " + handObj.name);
                EditorGUILayout.LabelField("demo: " + hts.preChangeRotations[HandTransformState.Finger.Index][0].x + " / " + hts.postChangeRotations[HandTransformState.Finger.Index][0].x);
                if (GUILayout.Button("See original"))
                {
                    hts.ApplyRotations(hts.preChangeRotations, handObj);
                }
                if (GUILayout.Button("Set modified"))
                {
                    hts.ApplyRotations(hts.postChangeRotations, handObj);
                }
                if (GUILayout.Button("EXPORT"))
                {
                    hts.teststring = "Hello!";
                    EditorUtility.SetDirty(hts);
                    SerializedObject so = new SerializedObject(hts);
                    
                    AssetDatabase.SaveAssets();
                    Debug.Log("Saved!");
                }
                break;
            default:
                EditorGUILayout.LabelField("Error: Something went wrong.");
                break;
        }
        
    }
}
