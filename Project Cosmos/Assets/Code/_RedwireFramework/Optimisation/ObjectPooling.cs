﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public StringValue tag;
        public GameObject prefab;
        public int size;
        [Space]
        [TextArea]
        
        public string developerDescription;
    }

    public List<Pool> pools;
    public static ObjectPooling instance;
    Dictionary<StringValue, Queue<GameObject>> poolDictionary;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        poolDictionary = new Dictionary<StringValue, Queue<GameObject>>();

        foreach(Pool pool in pools)
        {
            Queue<GameObject> objPool = new Queue<GameObject>();

            for(int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objPool.Enqueue(obj);
            }

            poolDictionary.Add(pool.tag, objPool);
        }
    }

    public GameObject SpawnFromPool(StringValue tag, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("Pool with tag " + tag.ToString() + " doesn't exist.");
            return null;
        }
            
        GameObject objToSpawn = poolDictionary[tag].Dequeue();
        objToSpawn.SetActive(true);
        objToSpawn.transform.position = position;
        objToSpawn.transform.rotation = rotation;

        poolDictionary[tag].Enqueue(objToSpawn);

        return objToSpawn;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
