﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyStats", menuName = "Redwire/Enemy Behaviour/EnemyStats")]
public class EnemyStats : ScriptableObject
{

    [Header("Overview")]
    public StringValue enemyName;
    [TextArea]
    public string description = "Beep bop i'm a description";

    [Header("Movement")]
    [Range(0,100)]
    public float moveSpeed = 5f;

    [Header("Combat")]
    public float maxHealth = 100f;
    [Range(0, 30)]
    public float viewDistance = 5f;
}
