﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroidStateAttack : BaseState
{
    private Droid droid;

    public DroidStateAttack(Droid droid) : base(droid.gameObject)
    {
        this.droid = droid;
    }

    public override Type Tick()
    {

        if(droid.target == null || droid.target.activeSelf == false)
        {
            return typeof(DroidStateIdle);
        }

        if(droid.RangeToTarget() <= droid.stats.viewDistance)
        {
            return typeof(DroidStateAttack);
            //Attack!!!!
        }

        return typeof(DroidStateIdle);
    }


}
