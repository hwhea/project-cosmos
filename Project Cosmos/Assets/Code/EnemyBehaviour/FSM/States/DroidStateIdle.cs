﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroidStateIdle : BaseState
{

    Droid droid;

    public DroidStateIdle(Droid droid) : base(droid.gameObject)
    {
        this.droid = droid;
    }

    public override Type Tick() 
    {

        //Change conditions:
        //Go to moving:
            // Pretty much always wants to be shooting or running, assess here if needs to be one of the two.

        if(!droid.HasTarget() || !droid.TargetInRange())
        {
            //Get me a node network to follow or Move to next node.
            return typeof(DroidStateNavigate);
        }
        else
        {
            return typeof(DroidStateAttack);
        }

        throw new NotImplementedException();
    }

 
}
