﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This state follows/ finds path.
public class DroidStateNavigate : BaseState
{
    Droid droid;
    AINavigator navigator;

    public DroidStateNavigate(Droid droid) : base(droid.gameObject)
    {
        this.droid = droid;
        this.navigator = droid.gameObject.GetComponent<AINavigator>();
        if (!this.navigator)
        {
            Debug.LogError("DroidStateNavigate constructor could not find navigator component on AI instance");
        }
    }

    public override Type Tick()
    {
        if (navigator.nodePath == null || navigator.nodePath.Count < 1)
        {
            //Get a path.
            navigator.SetNodePath(AIMasterController.DetermineFollowNodes());

            //If still null, just idle this unit.
            if (navigator.nodePath == null) 
                return typeof(DroidStateIdle);

            return typeof(DroidStateNavigate);
        }
        else
        {
            //We have a path.
            if (!navigator.hasFinishedNavigation)
            {
                //Do nothing, we're navigating at the minute.
                return typeof(DroidStateNavigate);
            }
            else
            {
                if (droid.HasTarget())
                {
                    return typeof(DroidStateAttack);
                }
                else
                {
                    //return typeof(DroidStateSetTarget);
                    return typeof(DroidStateIdle);
                }
                
            }
        }

        
    }
}
