﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameObject target;
    public EnemyStats stats;

    private void Start()
    {
        
    }

    public float RangeToTarget()
    {
        return Vector3.Distance(transform.position, target.transform.position);
    }

    public bool HasTarget()
    {
        return target != null && target.activeSelf == true;
    }

    public bool TargetInRange()
    {
        return RangeToTarget() >= stats.viewDistance;
    }
}