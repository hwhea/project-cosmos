﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(StateMachine))]
public class Droid : Enemy
{

    StateMachine sm => GetComponent<StateMachine>();
    

    // Start is called before the first frame update
    void Start()
    {
        InitStateMachine();
    }

    //Useful to have an init statemachine method.
    private void InitStateMachine()
    {
        var states = new Dictionary<Type, BaseState>()
        {
            {typeof(DroidStateIdle), new DroidStateIdle(this) },
            {typeof(DroidStateAttack), new DroidStateAttack(this) },
            {typeof(DroidStateNavigate), new DroidStateNavigate(this) }
        };

        //Pass the states variable we created and the 'default state' of the object.
        sm.SetStates(states, typeof(DroidStateIdle));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
