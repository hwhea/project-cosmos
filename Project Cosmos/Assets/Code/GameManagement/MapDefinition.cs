﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnDefinition
{
    public string name;

    public AnimationCurve curve = null;
    public Dictionary<DifficultyRating, MapDifficultyDefinition> levelSpawnData = new Dictionary<DifficultyRating, MapDifficultyDefinition>();
    public List<DifficultyRating> difficultyList = new List<DifficultyRating>();

    public SpawnDefinition(List<DifficultyRating> difficulties, int size)
    {
        difficultyList = difficulties;
        foreach (DifficultyRating difficulty in difficulties)
        {
            //Here we're adding a different level spawn definition for each level, which changes depending on the difficulty. 
            //Eg easier levels will want to spawn different amounts.
            List<LevelSpawnDefinition> lsds = new List<LevelSpawnDefinition>();
            for (int i = 0; i < size; i++)
            {
                lsds.Add(new LevelSpawnDefinition() { amountToSpawn = 0 });
            }
            levelSpawnData.Add(difficulty, new MapDifficultyDefinition() { levels = lsds, curve = AnimationCurve.Linear(0, 0, size, 300)});
        }

   }
}



[System.Serializable]
public class LevelSpawnDefinition
{
    public int amountToSpawn;
}

public class MapDifficultyDefinition
{
    public StringValue mapName;
    public StringValue mapDescription;

    public AnimationCurve curve = null;
    public List<LevelSpawnDefinition> levels;
}


