﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Difficulty", menuName = "Redwire/Game/New Difficulty Level")]

[System.Serializable]
public class DifficultyRating : ScriptableObject
{
    public StringValue name;
    public StringValue description;
}

[CreateAssetMenu(fileName = "New Difficulty", menuName = "Redwire/Game/New Difficulty Set")]

[System.Serializable]
public class LevelDifficulties : ScriptableObject
{

    public List<DifficultyRating> difficulties;
}

