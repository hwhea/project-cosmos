﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class MapDataGenerator : EditorWindow
{
    //Settings
    int curveElementHeight = 50;
    int curveListOffset = 120;
    Color color;
    AnimationCurve pointsDistributionCurve = AnimationCurve.Linear(0, 100, 1, 1000);

    [MenuItem("Redwire/Framework/Map Data Tool")]
    public static void ShowWindow()
    {
        GetWindow<MapDataGenerator>("Map Data Generator");
    }

    StringValue mapName;
    IntValue levelCount;
    public static LevelDifficulties diffSet;
    public static List<SpawnDefinition> spawnDefinitions = new List<SpawnDefinition>();

    void OnGUI()
    {
        GUILayout.Label("Create a New Map", EditorStyles.boldLabel);

        GUILayout.Space(10f);
        GUILayout.Label("Map Name:");
        mapName = (StringValue)EditorGUILayout.ObjectField(mapName, typeof(StringValue));

        GUILayout.Space(10f);
        GUILayout.Label("Level Count:");
        levelCount = (IntValue)EditorGUILayout.ObjectField(levelCount, typeof(IntValue));

        GUILayout.Space(10f);
        GUILayout.Label("Difficulty Set:");
        diffSet = (LevelDifficulties)EditorGUILayout.ObjectField(diffSet, typeof(LevelDifficulties));

        GUILayout.Space(10f);
        GUILayout.Label("Spawn Definitions", EditorStyles.boldLabel);

        List<SpawnDefinition> newList = new List<SpawnDefinition>();
        newList = spawnDefinitions;
        foreach (SpawnDefinition sd in spawnDefinitions)
        {
            EditorGUILayout.BeginHorizontal();
            sd.name = EditorGUILayout.TextField(sd.name);
            if (GUILayout.Button("Edit"))
            {
                //Edit code here
                EnemySpawnDefinitionWindow.LoadDefinition(sd);
            }
            if (GUILayout.Button("X"))
            {
                //Edit code here
                newList.Remove(sd);
            }
            EditorGUILayout.EndHorizontal();
        }
        spawnDefinitions = newList;

        if (GUI.Button(new Rect(3, position.height - 45, position.width - 6, 20), "Add Spawn Definition"))
        {
            spawnDefinitions.Add(new SpawnDefinition(diffSet.difficulties, levelCount.value));
            {
                name = "Droid";
            };
            EnemySpawnDefinitionWindow.ShowWindow();
        }

        if (GUI.Button(new Rect(3, position.height - 25, position.width-6, 20),"Create"))
        {
            EditorUtility.DisplayDialog("Yeah!","Created this map asset!", "Awesome!");
        }
    }

  

}