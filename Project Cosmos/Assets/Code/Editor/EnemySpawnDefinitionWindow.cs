﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EnemySpawnDefinitionWindow : EditorWindow
{
    //public static AnimationCurve spawnCurve = AnimationCurve.Linear(0, 0, 100, 300);


    public static List<AnimationCurve> spawnCurves = new List<AnimationCurve>();

    int curveElementHeight = 50;
    static SpawnDefinition thisSD;

    public static void ShowWindow()
    {
        GetWindow<EnemySpawnDefinitionWindow>("Enemy Spawn");
    }

    

    public static void LoadDefinition(SpawnDefinition sd)
    {
        spawnCurves.Clear();
        thisSD = sd;
        
        ShowWindow();
    }

    public void Init()
    {

    }


    int levelRep = 0;

    void OnGUI()
    {
        if(thisSD == null)
        {
            Close();
            return;
        }

        int counter = 1;
        foreach (DifficultyRating spawnDef in thisSD.difficultyList)
        {
            MapDifficultyDefinition mdd = null;
            thisSD.levelSpawnData.TryGetValue(spawnDef, out mdd);
            Debug.Log(spawnDef + " ");
            //Draw graph
            mdd.curve = EditorGUI.CurveField(
           new Rect(3, curveElementHeight * counter + 70, position.width - 6, curveElementHeight),
           mdd.curve, Color.red, new Rect(0, 0, thisSD.levelSpawnData.Count, 300));
            GUILayout.Label("Spawned at this level(" + levelRep + "): " + Mathf.Floor(mdd.curve.Evaluate((float)levelRep)));
            counter++;
        }

        levelRep = EditorGUILayout.IntSlider(levelRep, 0, thisSD.levelSpawnData.Count);
        
    }

    void SaveCurves()
    {

    }
}